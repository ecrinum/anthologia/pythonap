## Projet de visualisation des données AP 

Le projet demande l'utilisation de python3 et de python3-venv. `sudo apt install python3 python3-venv`

Ensuite on va créer un environnement virtuel où on installera flask et requests. L'environnement virtuel permet d'éviter d'avoir des problèmes de versions et de dépendances entre un projet et l'autre


```
git clone git@gitlab.huma-num.fr:ecrinum/anthologia/pythonap.git
cd pythonap
python3 -m venv venv
. venv/bin/activate
pip install Flask
pip install requests
 ```

Maintenant il faut faire tourner `fetch.py` pour récupérer en local le json du endpoint principal et le mettre en cache - sinon trop long: `python fetch.py`

On peut ensuite faire tourner le serveur: `python app.py` 

Vous pouvez ouvrir le projet dans votre navigateur, par défault à l'adresse 0.0.0.0:5000/

Les paths sont définie dans app.py. 
