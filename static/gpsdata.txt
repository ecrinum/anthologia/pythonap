lat	lon	title	description	icon	iconSize	iconOffset
32.6544208	35.6878856	Umm Qais,Gadara,Umm Qeis,	Ville de naissance de Meleager, Méléagre, Μελέαγρος,  et Philodemus of Gadara, Φιλόδημος ὁ Γαδαρεύς,  et  id ville:2	/static/img/marker.png	16,16	-8,-8
32.8079925	21.8661611	Cyrene,Κυρήνη Kyrēnē,	Ville de naissance de Callimachus, Καλλίμαχος, Callimaque,  et  id ville:3	/static/img/marker.png	16,16	-8,-8
31.2000995	29.9185702	Alexandria,Ἀλεξάνδρεια,Alexandrie,	Ville de naissance de Théon d'Alexandrie,  et  id ville:4	/static/img/marker.png	16,16	-8,-8
40.8059273	14.3468049	Ercolano,Herculaneum,	Ville de naissance de  id ville:5	/static/img/marker.png	16,16	-8,-8
39.1000356	26.5547483	Mytilene,Μυτιλήνη,	Ville de naissance de Alcaeus of Mytilene, Ἀλκαῖος ὁ Μυτιληναῖος, Alcée de Mytilène,  et Sappho, Σαπφώ,  et Crinagoras, Κριναγόρας,  et  id ville:6	/static/img/marker.png	16,16	-8,-8
33.560556	35.398056	Sidone,Σιδών,Sidon,	Ville de naissance de Antipater of Sidon, Ἀντίπατρος ὁ Σιδώνιος,  et  id ville:7	/static/img/marker.png	16,16	-8,-8
38.3637	27.136	Colophon,	Ville de naissance de Xenophanes,  et  id ville:8	/static/img/marker.png	16,16	-8,-8
None	Tenos,	Ville de naissance de  id ville:9	/static/img/marker.png	16,16	-8,-8
None	Locri,Locres,	Ville de naissance de Nossis,  et  id ville:11	/static/img/marker.png	16,16	-8,-8
None	Miletus,	Ville de naissance de  id ville:12	/static/img/marker.png	16,16	-8,-8
None	Halicarnasse,	Ville de naissance de  id ville:13	/static/img/marker.png	16,16	-8,-8
40.466667	 17.233333	Taranto,Taranto,Tarente,	Ville de naissance de Leonidas of Tarentum, Λεωνίδας ὁ Ταραντῖνος, Leonida di Taranto,  et  id ville:14	/static/img/marker.png	16,16	-8,-8
38.4842	 28.0407	Sardis,Sardes,Sardi,Σάρδιες,	Ville de naissance de Straton de Sardes, Straton de Sardes, Στράτων, Straton of Sardis,  et Diodoros Zonas de Sardes,  et  id ville:15	/static/img/marker.png	16,16	-8,-8
38.177222	 26.785	Teos,Téos,Τέως,	Ville de naissance de Anacreon of Teos,  et  id ville:16	/static/img/marker.png	16,16	-8,-8
None	Camiros,	Ville de naissance de Pisandre de Rhodes,  et  id ville:17	/static/img/marker.png	16,16	-8,-8
None	Samosate,	Ville de naissance de Lucien de Samosate,  et  id ville:18	/static/img/marker.png	16,16	-8,-8
None	Bène,	Ville de naissance de Rhianos,  et  id ville:19	/static/img/marker.png	16,16	-8,-8
None	Thèbes (Grèce),	Ville de naissance de Cratès de Thèbes,  et Perses,  et  id ville:20	/static/img/marker.png	16,16	-8,-8
None	Constantinople,Κωνσταντινούπολις,	Ville de naissance de Leontius Scholasticus, Λεόντιος ο σχολαστικός, Léontios le scholastique,  et Proclos,  et  id ville:21	/static/img/marker.png	16,16	-8,-8
None	Athènes,	Ville de naissance de Plato,  et  id ville:22	/static/img/marker.png	16,16	-8,-8
None	Samos,	Ville de naissance de Nicarchus,  et Aischrion, Eschrion,  et  id ville:23	/static/img/marker.png	16,16	-8,-8
40.38	 27.89	Cyzicus,Κύζικος,Cyzique,	Ville de naissance de Erycius, Erycios,  et  id ville:24	/static/img/marker.png	16,16	-8,-8
37.455333	 22.4205	Tegea,Τεγέα,Tégée,	Ville de naissance de Anyte from Tegea, Ἀνύτη, Anytè de Tégée,  et  id ville:25	/static/img/marker.png	16,16	-8,-8
38.418611	 27.139167	Smyrna,Σμύρνη,Smyrne,	Ville de naissance de Lollius Bassus,  et  id ville:27	/static/img/marker.png	16,16	-8,-8
37.623056	 24.336667	Keos,Κέως ,Céos,	Ville de naissance de Simonides, Σιμωνίδης, Simonide,  et  id ville:28	/static/img/marker.png	16,16	-8,-8
37.316667	 13.583333	Agrigento,Ἀκράγας,Agrigente,	Ville de naissance de  id ville:29	/static/img/marker.png	16,16	-8,-8
37.069167	 15.2875	Syracuse,	Ville de naissance de Theodoridas, Θεοδωρίδας ὁ Συρακούσιος, Théodoridas de Syracuse,  et  id ville:30	/static/img/marker.png	16,16	-8,-8
None	Byzantium,Βυζάντιον,Byzance,	Ville de naissance de Honestus, Όνεστου Βυζαντίου, Honestus de Byzance,  et  id ville:31	/static/img/marker.png	16,16	-8,-8
36.85	 27.233333	Cos,Κως,	Ville de naissance de  id ville:32	/static/img/marker.png	16,16	-8,-8
None	Agyrion,	Ville de naissance de Diodorus, Diodoros,  et  id ville:33	/static/img/marker.png	16,16	-8,-8
None	Éphèse,	Ville de naissance de Zenodotus,  et  id ville:34	/static/img/marker.png	16,16	-8,-8
None	Jérusalem,	Ville de naissance de  id ville:35	/static/img/marker.png	16,16	-8,-8
None	Antioche,	Ville de naissance de Archias d'Antioche,  et  id ville:36	/static/img/marker.png	16,16	-8,-8
None	Pella,	Ville de naissance de Posidippus,  et  id ville:37	/static/img/marker.png	16,16	-8,-8
None	Hermopolis,	Ville de naissance de Andronicus,  et  id ville:38	/static/img/marker.png	16,16	-8,-8
None	Thessalonique,	Ville de naissance de Philippus, Philippe de Thessalonique, Φίλιππος ὁ Θεσσαλονικεύς,  et  id ville:39	/static/img/marker.png	16,16	-8,-8
38.845278	 26.984444	Myrina,Μυρίνα,Myrina,	Ville de naissance de Agathias Scholasticus, Ἀγαθίας σχολαστικός, Agathias le scholastique,  et  id ville:40	/static/img/marker.png	16,16	-8,-8
26.566667	 31.75	Panopolis,Akhmîm,	Ville de naissance de Cyrus the poet, Κύρος ὁ Πανοπολίτης, Cyrus de Panopolis,  et  id ville:41	/static/img/marker.png	16,16	-8,-8
None	Calchis,	Ville de naissance de Euphorion,  Εὐφορίων ὁ Χαλκιδεύς,  et  id ville:42	/static/img/marker.png	16,16	-8,-8
None	Pleuron,	Ville de naissance de Alexandre l'Étolien,  et  id ville:43	/static/img/marker.png	16,16	-8,-8
