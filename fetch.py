# on va chercher les données pour les stocker localement
import requests
import os

r = requests.get(
    'http://anthologia.ecrituresnumeriques.ca/api/v1/entities/')

if not os.path.exists('./tmp'):
    os.makedirs('./tmp')

with open('./tmp/entities.tmp.json', 'w') as f:
  f.write(r.text)

os.rename('./tmp/entities.tmp.json', './tmp/entities.json')
