from flask import Flask, render_template, redirect 
import requests
import urllib.request, urllib.error
import json
import os
import re
import itertools
from operator import itemgetter

app = Flask(__name__, template_folder='./templates/')

# fonction pour construire la home page
@app.route('/') # route où seront servies ces données
def homepage(): # la fonction qui sert les données pour la route /
  with open('./tmp/entities.json', 'r') as r:
      entities=json.loads(r.read())
  rk = requests.get(
      'http://anthologia.ecrituresnumeriques.ca/api/v1/keywords/')
  return render_template('index.html', entities=entities, keywords=json.loads(rk.text))

@app.route('/entities/') # route où seront servies ces données
def allentities(): 
  with open('./tmp/entities.json', 'r') as r:
      entities=json.loads(r.read())
  ra = requests.get(
      'http://anthologia.ecrituresnumeriques.ca/api/v1/authors/')
  return render_template('entities.html', entities=entities, authors=json.loads(ra.text))

#entity
#todo: redondant avec l'autre route pour entités (urn). À effacer - avoir un redirect quand une entité n'a pas d'urn
@app.route('/entity/<int:entity_id>')# route pour chaque entité. la variable int:entity_id sera utilisée pour avoir une route pour chaque entité. (ex entity/1 entity/2 etc.)
def entities(entity_id):
  r = requests.get(
      'http://anthologia.ecrituresnumeriques.ca/api/v1/entities/' + str(entity_id))
  data = json.loads(r.text)
  if data['uris']:
        urn="urn:cts:greekLit:tlg7000.tlg001.ag:"+str(data['title'].replace("Greek Anthology ", ""))
  return redirect('/passages/'+urn, code=302)

# page mot-clé à refaire :
# todo :
@app.route('/keyword/<int:keyword_id>')
def keywords(keyword_id):
  rk = requests.get(
      'http://anthologia.ecrituresnumeriques.ca/api/v1/keywords/' + str(keyword_id))
  with open('./tmp/entities.json', 'r') as r:
      entities=json.loads(r.read())
  return render_template('keyword.html', keyword=json.loads(rk.text), entities=entities)

@app.route('/keywords')
def keywordslist():
  rk = requests.get(
      'http://anthologia.ecrituresnumeriques.ca/api/v1/keywords/')
  keywords=json.loads(rk.text)
  categories=[]
  for category in keywords:
    categories.append(category['category']['title'])  
  categories=list(set(categories))  
  categories=sorted(categories)
  return render_template('keywords.html', keywords=keywords, categories=categories)

#liste users: todo: retsructurer données dans un json à servir à jinja au lieu que l'html
# route users: mettre juste la liste des utilisateurs avec lien vers la page
# route users/id_user : Userr name, User istitution: contribtions: versions+updatedat draft+ updated_at + alignement+updatedat, scholie+updated at + authors+updatedat external_ref entity -> ordre inversé de date de modification + liens vers l'entité en question
@app.route('/users/')
def users():
  ru = requests.get(
      'http://anthologia.ecrituresnumeriques.ca/api/v1/users/')
  with open('./tmp/entities.json', 'r') as r:
        entities=json.loads(r.read())
  users=[]      
  for User in json.loads(ru.text):
        user='{'
        user+='"displayname":"'+str(User['displayName'])+'","istitution":"'+str(User['institution'])+'","iduser":"'+str(User['id_user'])+'"'
        for entity in entities:
          if entity['id_user']['id_user']==User['id_user']:
            if entity['id_user']['first_name']:
                user+=', "firstname":"'+str(entity['id_user']['first_name'])+'"'
            if entity['id_user']['last_name']:
                user+=', "lastname":"'+str(entity['id_user']['last_name'])+'"'
            break   
        user+='}'
        users.append(str(user))
  users=','.join(users)     
  users=json.loads('['+str(users)+']')
  return render_template('users.html', users=users)

@app.route('/users/<int:user_id>')
def user(user_id):
  with open('./tmp/entities.json', 'r') as r:
      entities=json.loads(r.read())
  ru = requests.get(
     'http://anthologia.ecrituresnumeriques.ca/api/v1/users/')
  for User in json.loads(ru.text):
      if User['id_user']==user_id:
        user='{'
        user+='"displayname":"'+str(User['displayName'])+'","istitution":"'+str(User['institution'])+'"'
        for entity in entities:
          if entity['id_user']['id_user']==user_id:
            if entity['id_user']['first_name']:
                user+=', "firstname":"'+str(entity['id_user']['first_name'])+'"'
            if entity['id_user']['last_name']:
                user+=', "lastname":"'+str(entity['id_user']['last_name'])+'"'
            break   
        user+=', "contributions":['
        # contributions array: [date, version/entitéetc, lienentité]
        contributions=[]
        for entity in entities:
          if entity['id_user']['id_user']==user_id:
              if entity['uris']:
                contributions.append('{"date":"'+str(entity['updatedAt'])+'","type":"entity", "uri":"'+str(entity['uris'][0]['value'])+'","title":"'+str(entity['title'])+'"}')
              
          for version in entity['versions']: 
            if version['id_user']==user_id:
              if entity['uris']:
                contributions.append('{"date":"'+str(version['updatedAt'])+'","type":"version", "uri":"'+str(entity['uris'][0]['value'])+'","title":"'+str(entity['title'])+'"}')
          for alignement in entity['alignements']: 
            if alignement['id_user']==user_id:
              if entity['uris']:
                contributions.append('{"date":"'+str(alignement['updatedAt'])+'","type":"alignement", "uri":"'+str(entity['uris'][0]['value'])+'","title":"'+str(entity['title'])+'"}')
          for note in entity['notes']: 
            if note['id_user']==user_id:
              if entity['uris']:
                contributions.append('{"date":"'+str(note['updatedAt'])+'","type":"note", "uri":"'+str(entity['uris'][0]['value'])+'","title":"'+str(entity['title'])+'"}')
          for extref in entity['externalRef']: 
            if extref['id_user']==user_id:
              if entity['uris']:
                contributions.append('{"date":"'+str(extref['updatedAt'])+'","type":"ExtRef", "uri":"'+str(entity['uris'][0]['value'])+'","title":"'+str(entity['title'])+'"}')
          for intref in entity['internalRef_sources']: 
            if intref['id_user']==user_id:
              if entity['uris']:
                contributions.append('{"date":"'+str(intref['updatedAt'])+'","type":"IntRef", "uri":"'+str(entity['uris'][0]['value'])+'","title":"'+str(entity['title'])+'"}')

          for keyword in entity['keywords']: 
           if keyword['id_user']==user_id:
             if entity['uris']:
               contributions.append('{"date":"'+str(keyword['updatedAt'])+'","type":"keyword '+str(keyword['id_keyword'])+'", "uri":"'+str(entity['uris'][0]['value'])+'","title":"'+str(entity['title'])+'"}')
          for image in entity['images']: 
           if image['id_user']==user_id:
             if entity['uris']:
               contributions.append('{"date":"'+str(image['updatedAt'])+'","type":"image '+str(image['id_image'])+'", "uri":"'+str(entity['uris'][0]['value'])+'","title":"'+str(entity['title'])+'"}')
        
        contributions=','.join(contributions)

      #  contributions= sorted(contributions, reverse=True)
        user+=str(contributions)      
        user+=']}'
        user=json.loads(str(user))
  return render_template('user.html',user=user)
# todo : restructurer données et servir un json à jinja
# limiter nombre d'item - les plus récents LISTE EN ORDRE DE DATE TOUT CONTENU CONFIoDU!
@app.route('/latest/')
def latest():
  with open('./tmp/entities.json', 'r') as r:
    entities=json.loads(r.read())
    ru = requests.get(
      'https://anthologia.ecrituresnumeriques.ca/api/v1/users/' )
    users=json.loads(ru.text)
    #array d'arrais que je veux obtenir: [["updated","versionouentityouwhatever(version de l'entité: entité: alignement de l'entité:","idouuripourlien", "username", "userid"],[...]] puis je sort par alphabétique
    #TODO:  ajouter notes, scholies, externalRef, imagesMan images, auteurs créés pis déplacer route latest
    data=[]
    for entity in entities:
      subdata=[]
      subdata.append(entity['updatedAt']) 
      subdata.append('entité') 
      subdata.append(entity['title']) 
      subdata.append("urn:cts:greekLit:tlg7000.tlg001.ag:"+str(entity['title'].replace("Greek Anthology ", ""))) 
      subdata.append(entity['id_user']['id_user']) 
      for user in users: 
        if user['id_user']==entity['id_user']['id_user']:
          subdata.append(user['displayName'])            
      data.append(subdata)
      if entity['versions']:
        for version in entity['versions']:
          subdata=[]
          subdata.append(version['updatedAt']) 
          subdata.append('version de') 
          subdata.append(entity['title'])
          if entity['uris']:
              subdata.append(entity['uris'][0]['value'].replace("http://data.perseus.org/citations/", "")) 
          subdata.append(version['id_user']) 
          for user in users: 
            if user['id_user']==version['id_user']:
              subdata.append(user['displayName'])            
          data.append(subdata)
      if entity['alignements']:
        for alignement in entity['alignements']:
          subdata=[]
          subdata.append(alignement['updatedAt']) 
          subdata.append('alignement de') 
          subdata.append(entity['title']) 
          if entity['uris']:
              subdata.append(entity['uris'][0]['value'].replace("http://data.perseus.org/citations/", "")) 
          subdata.append(alignement['id_user']) 
          for user in users: 
            if user['id_user']==alignement['id_user']:
              subdata.append(user['displayName'])            
          data.append(subdata)
  return render_template('latest.html', data=data)

@app.route('/scholia/')
def scholia():
  rs = requests.get(
      'http://anthologia.ecrituresnumeriques.ca/api/v1/scholies/')
  return render_template('scholies.html', scholies=json.loads(rs.text))

@app.route('/scholia/<urn>')
def scholion(urn):
  rs = requests.get(
      'http://anthologia.ecrituresnumeriques.ca/api/v1/scholies/')
  scholia=json.loads(rs.text)
  urnmatch= re.match( r'(urn:cts:greekLit:tlg5011.tlg001.sag:)(.*)', urn, re.M|re.I)
  urntitle=urnmatch.group(2)
  print (urntitle)
  for scholion in scholia:
      title= scholion['title']
      if title == urntitle:
          rso= requests.get('http://anthologia.ecrituresnumeriques.ca/api/v1/scholies/'+str(scholion['id_scholie']))
  urnmatch2= re.match( r'(urn:cts:greekLit:tlg5011.tlg001.sag:)([0-9]*).(.*)', urn, re.M|re.I)
  book=urnmatch2.group(2)
  return render_template('scholion.html', scholion=json.loads(rso.text), book=book)

@app.route('/maps/')
def maps():
  rc = requests.get(
      'http://anthologia.ecrituresnumeriques.ca/api/v1/cities/')
  cities=json.loads(rc.text)
  ra = requests.get(
      'http://anthologia.ecrituresnumeriques.ca/api/v1/authors/')
  authors=json.loads(ra.text)
  geodata = "lat	lon	title	description	icon	iconSize	iconOffset\n"
  for city in cities:
    idcity=city['id_city']
    gpsa=city['GPS']
    gps=str(gpsa).replace(',','\t')
    citynames=""
    for cityname in city['versions']:
      citynames+=cityname['name'] + ','
      names=""
      for author in authors:
        if 'city_born' not in author:
          continue
        else:
          if author['city_born']['id_city'] is idcity:
            for name in author['versions']:
              names+= name['name'] + ', '
            names+=" et "
    geodata += str(gps) + '\t' + str(citynames) + '\t' + 'Ville de naissance de ' + str(names) + ' id ville:' + str(idcity) + '\t' + '/static/img/marker.png\t16,16\t-8,-8\n'

  
  
  with open('./static/gpsdata.tmp.txt', 'a') as f:
    f.write(geodata)
  
  os.rename('./static/gpsdata.tmp.txt', './static/gpsdata.txt')
  return render_template('maps.html')

#Voici la bonne route pour les entités
@app.route('/passages/<urn>')
def entityurn(urn):
  if 'urn:cts:greekLit:tlg7000.tlg001' in urn:
    with open('./tmp/entities.json', 'r') as r:
        entities=json.loads(r.read())
        data=''
        authors=''
        matchUrn = re.match( r'(urn:cts:greekLit:tlg7000.tlg001).(ag|perseus-grc.):([0-9]*).(.*)', urn, re.M|re.I)
        print (matchUrn)
        bookurn = matchUrn.group(3)
        fragmenturn = matchUrn.group(4)
        # print (bookurn)
        for entity in entities:
          title = entity['title']
          # print (title)
          if 'Greek Anthology' in str(entity['title']):
            matchObj = re.match( r'([a-zA-Z\s]*)\s([0-9]*).(.*)', title, re.M|re.I)
            book = matchObj.group(2)
            fragment = matchObj.group(3)
            if book == bookurn and fragment == fragmenturn: 
              identity= entity['id_entity'] 
              rent = requests.get('http://anthologia.ecrituresnumeriques.ca/api/v1/entities/' + str(identity))
              data = json.loads(rent.text) 
              
              if len(data['authors'])==0:
                  authors=json.loads('[{"name":"Anonyme"}]')
              else:
                for author in data['authors']:
                  idauthor = author['id_author']
                  ra = requests.get(
                  'http://anthologia.ecrituresnumeriques.ca/api/v1/authors/' + str(idauthor)
                  )
                  authors= json.loads(ra.text)['versions']
              if len(data['keywords'])==0:
                  keywords=''# il doit y avoir une manière plus élégante de le faire
              else:
                keywords=[]  
                for keyword in data['keywords']:
                  idkeyword = keyword['id_keyword']
                  rk = requests.get(
                  'http://anthologia.ecrituresnumeriques.ca/api/v1/keywords/' + str(idkeyword)
                  )
                  keywordsdata = json.loads(rk.text)
                  for version in keywordsdata['versions']:
                    keywords.append('<a href="/keyword/'+ str(idkeyword) + '">' + str(version['title']) + '</a>')
                keywords= ', '.join(keywords)    
              notes=[]  
              if len(data['notes'])==0:
                continue
              else:
                for note in data['notes']:
                  idnote= note['id_note']  
                  rn = requests.get(
                  'http://anthologia.ecrituresnumeriques.ca/api/v1/notes/' + str(idnote)
                  )
                  notes.append(json.loads(rn.text))
    return render_template('entityurn.html', book=bookurn, prevf=int(fragmenturn)-1, nextf=int(fragmenturn)+1, entitydata=data, authors=authors, keywords=keywords, notes=notes)
  elif 'urn:cts:greekLit:tlg5011.tlg001.sag:' in urn:
    rs = requests.get(
      'http://anthologia.ecrituresnumeriques.ca/api/v1/scholies/')
    scholia=json.loads(rs.text)
    urnmatch= re.match( r'(urn:cts:greekLit:tlg5011.tlg001.sag:)(.*)', urn, re.M|re.I)
    urntitle=urnmatch.group(2)
    # print (urntitle)
    for scholion in scholia:
        title= scholion['title']
        if title == urntitle:
            rso= requests.get('http://anthologia.ecrituresnumeriques.ca/api/v1/scholies/'+str(scholion['id_scholie']))
    urnmatch2= re.match( r'(urn:cts:greekLit:tlg5011.tlg001.sag:)([0-9]*).(.*)', urn, re.M|re.I)
    book=urnmatch2.group(2)
    return render_template('scholion.html', scholion=json.loads(rso.text), book=book)
  else:
      return render_template('404.html')

# route pour orgasiner les épigrammes par livre. TODO: il faudrait les ordonner par int et pas par string, mais je ne sais pas le faire
@app.route('/books/')
def books():
  with open('./tmp/entities.json', 'r') as r:
      entities=json.loads(r.read())
      alltitles=[]
      for entity in entities:
          if 'Greek Anthology' in str(entity['title']) and entity['uris']:  
              alltitles.append('{\"title\":\"' + str(entity['title']) + '\",\"uri\":\"' + str(entity['uris'][0]['value']) + '\"}')
      alltitles=','.join(alltitles)
      alltitles=json.loads('[' + str(alltitles) + ']')
      print(alltitles)
      titles=[]
      for index, obj in enumerate(alltitles):
        title = obj['title']  
        uri = obj['uri']
        matchObj = re.match( r'([a-zA-Z\s]*)\s([0-9]*).(.*)', title, re.M|re.I)

        if matchObj:
            titles.append('{\"work\":\"'+ str(matchObj.group(1)) + '\", \"book\":\"' + str(matchObj.group(2)) + '\", \"fragment\":\"' + str(matchObj.group(3)) + '\",\"uri\":\"' + str(uri) + '\"}') 
        else:
           print("No match!!") 
      titles=','.join(titles)
      titles=json.loads('['+str(titles)+']')  
      titles = sorted(titles, key=itemgetter('book'))
      mytitles=[]
      for key, value in itertools.groupby(titles, key=itemgetter('book')):
        mytitle='{"book":"' + str(key) + '","body":['
        arraybook=[]
        for i in value:
          arraybook.append('{"fragment":"' + str(i.get('fragment')) + '","uri":"' +str(i.get('uri')) + '"}')
        arraybook=  ','.join(arraybook)
        mytitle+=str(arraybook) + ']}'  
        mytitles.append(mytitle)
      mytitles= ','.join(mytitles)  
      mytitles=json.loads('['+str(mytitles)+']')
  return render_template('books.html', titles=mytitles)

@app.route('/authors/')
def authors():
  ra = requests.get(
      'https://anthologia.ecrituresnumeriques.ca/api/v1/authors')
  authors=json.loads(ra.text)
  nameslist=[]
  for author in authors:
    try:
        nameslist.append(author['versions'][0]['name'])
    except:
        print(author) # authorsdata=[] 
  nameslist=sorted(nameslist)
  authors_epigrams_list=[]
  for name in nameslist:
      for author in authors:
          try:
              if author['versions'][0]['name'] == name:
                  authors_epigrams_list.append(author)
          except:
              continue
  # for author in authors:
  #     authordict={'id': author['id_author'], 'versions': versions
  #     for version in author['versions']:

  return render_template('authors.html',authors=authors_epigrams_list)

@app.route('/authors/<int:author_id>')
def author(author_id):
  ra = requests.get(
      'https://anthologia.ecrituresnumeriques.ca/api/v1/authors/' + str(author_id) )
  with open('./tmp/entities.json', 'r') as r:
      entities=json.loads(r.read())
  return render_template('author.html',author=json.loads(ra.text),entities=entities)

@app.route('/codex')
def codex():
  with open('./tmp/entities.json', 'r') as r:
    entities=json.loads(r.read())
    numimages=0
    for entity in entities:
      for image in entity['imagesManuscript']:
          numimages=numimages+1
          url = image['URL']
          # try:
          #   conn = urllib.request.urlopen(url)
          # except urllib.error.HTTPError as e:
    # # Return code error (e.g. 404, 501, ...)
    # # ...
          #   print('HTTPError: {}'.format(e.code))
          # except urllib.error.URLError as e:
    # # Not an HTTP-specific error (e.g. connection refused)
    # # ...
          #   print('URLError: {}'.format(e.reason))
          # else:
    # # 200
    # # ...
          #   print('good')
  return render_template('codex.html',entities=entities, numimages=str(numimages))

@app.route('/bibliography/')
def bibliography():
  rb = requests.get(
      'https://api.zotero.org/groups/2484902/items?include=data,citation')
  return render_template('bibliography.html',bibliography=json.loads(rb.text))

@app.route('/test/')
def test():
  with open('./tmp/entities.json', 'r') as r:
    entities=json.loads(r.read())
    ru = requests.get(
      'https://anthologia.ecrituresnumeriques.ca/api/v1/users/' )
    users=json.loads(ru.text)
    #array d'arrais que je veux obtenir: [["updated","versionouentityouwhatever(version de l'entité: entité: alignement de l'entité:","idouuripourlien", "username", "userid"],[...]] puis je sort par alphabétique
    #TODO:  ajouter notes, scholies, externalRef, imagesMan images, auteurs créés pis déplacer route latest
    data=[]
    for entity in entities:
      subdata=[]
      subdata.append(entity['updatedAt']) 
      subdata.append('entité') 
      subdata.append(entity['title']) 
      if entity['uris']:
          subdata.append(entity['uris'][0]['value'].replace("http://data.perseus.org/citations/", "")) 
      subdata.append(entity['id_user']['id_user']) 
      for user in users: 
        if user['id_user']==entity['id_user']['id_user']:
          subdata.append(user['displayName'])            
      data.append(subdata)
      if entity['versions']:
        for version in entity['versions']:
          subdata=[]
          subdata.append(version['updatedAt']) 
          subdata.append('version de') 
          subdata.append(entity['title'])
          if entity['uris']:
              subdata.append(entity['uris'][0]['value'].replace("http://data.perseus.org/citations/", "")) 
          subdata.append(version['id_user']) 
          for user in users: 
            if user['id_user']==version['id_user']:
              subdata.append(user['displayName'])            
          data.append(subdata)
      if entity['alignements']:
        for alignement in entity['alignements']:
          subdata=[]
          subdata.append(alignement['updatedAt']) 
          subdata.append('alignement de') 
          subdata.append(entity['title']) 
          if entity['uris']:
              subdata.append(entity['uris'][0]['value'].replace("http://data.perseus.org/citations/", "")) 
          subdata.append(alignement['id_user']) 
          for user in users: 
            if user['id_user']==alignement['id_user']:
              subdata.append(user['displayName'])            
          data.append(subdata)
  return render_template('test.html',data=data)

if __name__ == '__main__':
  app.run(host='0.0.0.0', debug=True)
